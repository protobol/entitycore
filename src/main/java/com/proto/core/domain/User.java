package com.proto.core.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity(name = "users")
public class User extends BaseEntityAudit {

    @NotNull
    @Size(min = 1)
    @Column(unique = true, nullable = false)
    private String email;

    @NotNull
    @Size(min = 1)
    @Column(nullable = false)
    private String password;

    public User() {
        super();
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
